$(document).ready(function () {
    // place JS code here

    $('a.a-tombol').click(function (e) { 
        $('a.a-tombol').css('color', 'black');
        $(this).css('color', '#658D75');
    });


    $('a.a-tombol').click(function (e) { 
        if (opened) { 
            $('#first').removeClass('rotate-first-clicked');
            $('#first').addClass('rotate-first-unclicked');
            setTimeout(() => {
                $('#middle').css('visibility', 'visible');
            }, 175);
            $('#second').removeClass('rotate-second-clicked');
            $('#second').addClass('rotate-second-unclicked');
            opened = false;

            $('.arrow').css('z-index', '1');
            $('#container-tombol').slideUp('slow');
        }

        else if (($(this).text() == 'BERANDA') || ($(this).text() == 'TENTANG LDMK')) {
            $('#hide').slideUp('slow');
        }
    });

    $('#menu').hover(function () {
            // over
            $('#hide').slideDown('slow');
        }, function () {
            // out
            $('#hide').slideUp('slow');
        }
    );


    var opened = false;
    $('.burger-nav').click(function (e) {
        if (opened) {
            $('#first').removeClass('rotate-first-clicked');
            $('#first').addClass('rotate-first-unclicked');
            setTimeout(() => {
                $('#middle').css('visibility', 'visible');
            }, 175);
            $('#second').removeClass('rotate-second-clicked');
            $('#second').addClass('rotate-second-unclicked');
            opened = false;

            $('.arrow').css('z-index', '1');
            $('#container-tombol').slideUp('slow');
        } else {
            setTimeout(() => {
                $('#middle').css('visibility', 'hidden');                
            }, 175);
            $('#first').addClass('rotate-first-clicked');
            $('#first').removeClass('rotate-first-unclicked');
            $('#second').addClass('rotate-second-clicked');
            $('#second').removeClass('rotate-second-unclicked');
            opened = true;

            $('.arrow').css('z-index', '-1');
            $('#container-tombol').slideDown('slow');
        }
    });

    


    var currentSelectorOpened = null;


    $('.kegiatan-card').click(function (e) {
        var idDiv = $(this).attr('id');
        var selectorPopUp = '#'+idDiv+".detil-kegiatan";

        if (currentSelectorOpened !== null) {
            $(currentSelectorOpened).css('display','none');
        }
        $(selectorPopUp).css('display', 'block');
        currentSelectorOpened = selectorPopUp;
    });

    $('.close-detil').click(function (e) { 
        $(currentSelectorOpened).css('display','none');
        currentSelectorOpened = null;
    });
    
    $('.tentang-arrow').click(function (e) {
        if ($('.tentang-container').length) {
            $('html, body').animate({
                scrollTop: $('.tentang-container').offset().top
            }, 750)   
        } else {
            window.location = '/';
        }
    });

    $('#icon-booklet').click(function (e) { 
        $('.hidden')[0].click();
    });
});