from django.shortcuts import render
from django.http import HttpResponse
from app.models import *

# Create your views here.

def index(request):
    kegiatan = Kegiatan.objects.all()
    data = {'kegiatan':kegiatan}
    return render(request, 'app/Welcome.html', data)

def nyoba(request):
    info = Info.objects.all()
    kegiatan = Kegiatan.objects.all()
    data = {'info':info, 'kegiatan':kegiatan}
    for i in kegiatan:
        print(i.foto.url)
    return render(request, 'app/nyoba.html', data)

def halaman_menu(request):
    return render(request, 'app/halaman_menu.html')

def halaman_chant(request):
    return render(request, 'app/halaman_chant.html')

def halaman_identitas(request):
    return render(request, 'app/halaman_identitas.html')

def halaman_reminder(request):
    return render(request, 'app/halaman_reminder.html')

def halaman_narasi(request):
    return render(request, 'app/halaman_narasi.html')

def halaman_panduan(request):
    return render(request, 'app/halaman_panduan.html')

def halaman_upcoming(request):
    return render(request, 'app/halaman_upcoming.html')