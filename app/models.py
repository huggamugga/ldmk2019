from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    nama = models.CharField(max_length=100)
    tanggal = models.DateField(auto_now=False)
    waktu = models.TimeField(auto_now=False)
    deskripsi = models.TextField()
    foto = models.ImageField(upload_to='kegiatan/%Y%m%d/', blank=True)

    class Meta:
       ordering = ['-tanggal', '-waktu']

    def __str__(self):
        return self.nama


class Info(models.Model):
    judul = models.CharField(max_length=100)
    waktu_published = models.DateTimeField(auto_now=True)
    isi = models.TextField()

    def __str__(self):
        return self.judul