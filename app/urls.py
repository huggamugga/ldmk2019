from django.urls import path

from . import views

app_name='app'
urlpatterns = [
    path('', views.index, name='index'),
    path('nyoba', views.nyoba, name='index'),
    path('menu', views.halaman_menu, name='halaman_menu'),
    path('menu/identitas-kami', views.halaman_identitas, name='halaman_identitas'),
    path('menu/just-reminder', views.halaman_reminder, name='halaman_reminder'),
    path('menu/narasi-harian', views.halaman_narasi, name='halaman_narasi'),
    path('menu/panduan-mu', views.halaman_panduan, name='halaman_panduan'),
    path('menu/upcoming-event', views.halaman_upcoming, name='halaman_upcoming'),
    path('menu/chant', views.halaman_chant, name='halaman_chant'),
]
